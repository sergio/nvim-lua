-- File: ~/.config/nvim/init.lua
-- Last Change: Tue, 31 May 2022 21:22
-- https://github.com/LunarVim/LunarVim/issues/1418#issuecomment-907789018

vim.cmd [[
  syntax off
  filetype off
  filetype plugin indent off
]]

vim.opt.shadafile = "NONE"
-- vim.g.loaded_gzip = false
-- vim.g.loaded_netrwPlugin = false

local core_modules = {
    -- "core.plugins",
    "core.settings",
    "core.autocmd",
    "core.mappings",
}

-- Using pcall we can handle better any loading issues
for _, module in ipairs(core_modules) do
    local ok, err = pcall(require, module)
    if not ok then
        error("Error loading " .. module .. "\n\n" .. err)
    end
end

vim.defer_fn(function()
  require "core.plugins"
  vim.opt.shadafile = ""
  vim.cmd [[
    rshada!
    doautocmd BufRead
    syntax on
    filetype on
    filetype plugin indent on
    PackerLoad nvim-treesitter
  ]]
  vim.defer_fn(function()
    vim.cmd [[
      PackerLoad plenary.nvim
      PackerLoad popup.nvim
      PackerLoad nvim-lspconfig
      silent! bufdo e
    ]]
  end, 15)
end, 5)
