-- autocmd.lua
-- Last Change: Mon, 06 Jun 2022 - 14:34

-- https://bit.ly/3x8oiNR

local augroups = {}

augroups.buf_write_pre = {

    mkdir_before_saving = {
        event = {"BufWritePre", "FileWritePre"},
        pattern = "*",
        -- TODO: Replace vimscript function
        command = [[ silent! call mkdir(expand("<afile>:p:h"), "p") ]],
    },

    trim_extra_spaces_and_newlines = {
        event = "BufWritePre",
        pattern = "*",
        -- TODO: Replace vimscript function
        command = require("core.utils").preserve('%s/\\s\\+$//ge'),
    },

}

augroups.misc = {
	highlight_yank = {
		event = "TextYankPost",
		pattern = "*",
		callback = function ()
			vim.highlight.on_yank{higroup="IncSearch", timeout=400, on_visual=true}
		end,
	},
	-- trigger_nvim_lint = {
	-- 	event = {"BufEnter", "BufNew", "InsertLeave", "TextChanged"},
	-- 	pattern = "<buffer>",
	-- 	callback = function ()
	-- 		require("lint").try_lint()
	-- 	end,
	-- },
	unlist_terminal = {
		event = "TermOpen",
		pattern = "*",
		callback = function ()
			vim.opt_local.buflisted = false
            vim.opt.listchars=''
            vim.cmd('startinsert')
		end
	},
}
